<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <title>Funkcje JSTL</title>
</head>
<body>
<c:if test="${not empty param.name}">
    <c:out value="${fn:toUpperCase(param.name)}"/>
</c:if>

<h3>
    W nagłówku jest <c:out value="${fn:length(header)}"/> elementów:
</h3>

<c:forEach var="headItem" items="${header}">
    <c:out value="${fn:toUpperCase(headItem.key)}"/>:<c:out value="${fn:toLowerCase(headItem.value)}"/>
    <br>
</c:forEach>

<br>
<c:set var="strings" value="${fn:split('Ala ma kota zmieniana na tablicę', ' ') }"/>
<c:set var="joined" value="${fn:join(strings, '--') }"/>
<c:out value="${joined }" />
<br>
<c:if test="${fn:startsWith(joined, 'Ala') }">
    Hura!
</c:if>
<c:out value="${fn:replace(joined, 'Ala', 'Karol') }" />

</body>
</html>
